**Installation**  
**1.** add this to composer.json
```
    "repositories": [  
        {"type": "composer", "url": "https://pelkas.repo.repman.io"}  
    ]  
```
**2.** composer require coaladmin/five-columns-three-with-header-and-text-bundle  
**3.** bin/console coaladmin:create-FiveColumnsThreeWithHeaderAndTextBundle  
**4.** bin/console assets:install

**Example**
![](src/Resources/public/ExamplePicture.png)