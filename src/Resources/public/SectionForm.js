import React, {useRef, useState} from 'react';
import {useForm} from "react-hook-form";

const SectionForm = ({item, currentMenuItem,sectionList,pagesList}) => {
    const [showForm, setShowForm] = useState(false)
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [fields, setFields] = useState(null);


    const {register, control, formState: {errors}} = useForm();
    const [title, setTitle] = useState(item?.title ?? '');
    const [mainDescription, setMainDescription] = useState(item?.mainDescription ?? '');

    const [firstTileFirstText, setFirstTileFirstText] = useState(item?.firstTileFirstText ?? '');
    const [firstTileSecondText, setFirstTileSecondText] = useState(item?.firstTileSecondText ?? '');

    const [secondTileFirstText, setSecondTileFirstText] = useState(item?.secondTileFirstText ?? '');
    const [secondTileSecondText, setSecondTileSecondText] = useState(item?.secondTileSecondText ?? '');

    const [thirdTileFirstText, setThirdTileFirstText] = useState(item?.thirdTileFirstText ?? '');
    const [thirdTileSecondText, setThirdTileSecondText] = useState(item?.thirdTileSecondText ?? '');

    const form = useRef(null);
    const [bundleName, setBundleName] = useState('FiveColumnsThreeWithHeaderAndTextBundle')


    const [send, setSend] = useState(null);
    const [pageSectionId, setPageSectionId] = useState(null)
    const resource = '/api/page_sections';


    const onSubmit = (e) => {

        e.preventDefault()
        const currentSection = sectionList.find(el => el.title === bundleName);
        const currentPage = pagesList.find((el) => {
            var array = el.menuItem.split('/')
            if (array[array.length - 1] == currentMenuItem) {
                return el.id
            }
        })

        const dataToSend = {
            page: `/api/pages/${currentPage.id}`,
            section: `/api/sections/${currentSection.id}`,
            params: [
                {
                    elements: [
                        {
                            id: 1,
                            name: "Nagłówek",
                            text: title
                        },
                        {
                            id: 2,
                            name: "Główny opis",
                            text: mainDescription
                        },
                        {
                            id: 3,
                            name: "Pierwszy box",
                            firstText: firstTileFirstText,
                            secondText: firstTileSecondText
                        },
                        {
                            id: 4,
                            name: "Drugi box",
                            firstText: secondTileFirstText,
                            secondText: secondTileSecondText
                        },
                        {
                            id: 5,
                            name: "Trzeci box",
                            firstText: thirdTileFirstText,
                            secondText: thirdTileSecondText
                        },
                    ]
                }
            ],
        }

        fetch(`${window.location.origin}/api/page_sections`, {
            method: 'POST',
            body: JSON.stringify(dataToSend),
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                window.location.reload(true)
                return response.json()
            })
            .then(data => {
            })
            .catch((error) => {
            });



    }


    return (
        <form method={'POST'} ref={form} className='add_section_text_with_image' onSubmit={onSubmit}>
            <div className="form-control">
                <label>Nagłówek</label>
                <input type="text" {...register("title")}
                       value={title} onChange={(e) => setTitle(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Treść</label>
                <input {...register("mainDescription")}
                       value={mainDescription} onChange={(e) => setMainDescription(e.target.value)}
                />
            </div>
            <hr />
            <div className="form-control">
                <label>Liczba</label>
                <input type="text" name="firstTileFirstText" {...register("firstTileFirstText")}
                       value={firstTileFirstText} onChange={(e) => setFirstTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="firstTileSecondText" {...register("firstTileSecondText")}
                       value={firstTileSecondText} onChange={(e) => setFirstTileSecondText(e.target.value)}
                />
            </div>
            <hr />
            <div className="form-control">
                <label>Liczba</label>
                <input type="text" name="secondTileFirstText" {...register("secondTileFirstText")}
                       value={secondTileFirstText} onChange={(e) => setSecondTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="secondTileSecondText" {...register("secondTileSecondText")}
                       value={secondTileSecondText} onChange={(e) => setSecondTileSecondText(e.target.value)}
                />
            </div>
            <hr />
            <div className="form-control">
                <label>Liczba</label>
                <input type="text" name="thirdTileFirstText" {...register("thirdTileFirstText")}
                       value={thirdTileFirstText} onChange={(e) => setThirdTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="thirdTileSecondText" {...register("thirdTileSecondText")}
                       value={thirdTileSecondText} onChange={(e) => setThirdTileSecondText(e.target.value)}
                />
            </div>
            <button id="submit" type="submit" className="btn btn-block"><i
                className="far fa-save"> </i> Zapisz
            </button>
        </form>

    )
}

export default SectionForm