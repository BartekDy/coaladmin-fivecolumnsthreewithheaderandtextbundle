import React, {useRef, useState, useEffect} from 'react';
import {useForm} from "react-hook-form";

const EditForm = ({id}) =>{

    const [data, setData] = useState(null)
    const [showForm, setShowForm] = useState(false)
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [fields, setFields] = useState(null);


    const {register, control, formState: {errors}} = useForm();
    const [heading, setHeading] = useState(null);
    const [description, setDescription] = useState(null);

    const [firstTileFirstText, setFirstTileFirstText] = useState(null);
    const [firstTileSecondText, setFirstTileSecondText] = useState(null);

    const [secondTileFirstText, setSecondTileFirstText] = useState(null);
    const [secondTileSecondText, setSecondTileSecondText] = useState(null);

    const [thirdTileFirstText, setThirdTileFirstText] = useState(null);
    const [thirdTileSecondText, setThirdTileSecondText] = useState(null);

    const form = useRef(null);

    useEffect(() => {
        fetch(`${window.location.origin}/api/page_sections/${id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                return response.json()
            })
            .then(data => {
                data.params[0].elements.map((el=> {
                    switch (el.name) {
                        case 'Nagłówek':
                            return setHeading(el.text)
                        case 'Główny opis':
                            return setDescription(el.text)
                        case 'Pierwszy box':
                            setFirstTileFirstText(el.firstText)
                            return setFirstTileSecondText(el.secondText)
                        case 'Drugi box':
                            setSecondTileFirstText(el.firstText)
                            return setSecondTileSecondText(el.secondText)
                        case 'Trzeci box':
                            setThirdTileFirstText(el.firstText)
                            return setThirdTileSecondText(el.secondText)
                        default:
                            return null
                    }
                }))
            })
            .catch((error) => {
            });
    },[])

    const onSubmit = (e) => {
        e.preventDefault()
        const data = [
            {
                elements: [
                    {
                        id: 1,
                        name: "Nagłówek",
                        text: heading
                    },
                    {
                        id: 2,
                        name: "Główny opis",
                        text: description
                    },
                    {
                        id: 3,
                        name: "Pierwszy box",
                        firstText: firstTileFirstText,
                        secondText: firstTileSecondText
                    },
                    {
                        id: 4,
                        name: "Drugi box",
                        firstText: secondTileFirstText,
                        secondText: secondTileSecondText
                    },
                    {
                        id: 5,
                        name: "Trzeci box",
                        firstText: thirdTileFirstText,
                        secondText: thirdTileSecondText
                    },
                ]
            }
        ]

        const DataToSent = {
            op:'replace',
            path: 'page_sections/params',
            params: data
        }

        fetch(`${window.location.origin}/api/page_sections/${id}`, {
            method: 'PATCH',
            body: JSON.stringify(DataToSent),
            headers: {
                "Content-Type": "application/merge-patch+json"
            }
        })
            .then((response) => {
                window.location.reload(true)
                return response.json()
            })
            .then(data => {
            })
            .catch((error) => {
            });
    }
    return(
        <form method={'POST'} ref={form} className='add_section_text_with_image' onSubmit={onSubmit}>
            <div className="form-control">
                <label>Nagłówek</label>
                <input type="text" {...register("title")}
                       value={heading} onChange={(e) => setHeading(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Treść</label>
                <input {...register("mainDescription")}
                       value={description} onChange={(e) => setDescription(e.target.value)}
                />
            </div>
            <hr />
            <div className="form-control">
                <label>Liczba</label>
                <input type="text" name="firstTileFirstText" {...register("firstTileFirstText")}
                       value={firstTileFirstText} onChange={(e) => setFirstTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="firstTileSecondText" {...register("firstTileSecondText")}
                       value={firstTileSecondText} onChange={(e) => setFirstTileSecondText(e.target.value)}
                />
            </div>
            <hr />
            <div className="form-control">
                <label>Liczba</label>
                <input type="text" name="secondTileFirstText" {...register("secondTileFirstText")}
                       value={secondTileFirstText} onChange={(e) => setSecondTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="secondTileSecondText" {...register("secondTileSecondText")}
                       value={secondTileSecondText} onChange={(e) => setSecondTileSecondText(e.target.value)}
                />
            </div>
            <hr />
            <div className="form-control">
                <label>Liczba</label>
                <input type="text" name="thirdTileFirstText" {...register("thirdTileFirstText")}
                       value={thirdTileFirstText} onChange={(e) => setThirdTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="thirdTileSecondText" {...register("thirdTileSecondText")}
                       value={thirdTileSecondText} onChange={(e) => setThirdTileSecondText(e.target.value)}
                />
            </div>
            <button id="submit" type="submit" className="btn btn-block"><i
                className="far fa-save"> </i> Zapisz
            </button>
        </form>
    )
}
export default EditForm;