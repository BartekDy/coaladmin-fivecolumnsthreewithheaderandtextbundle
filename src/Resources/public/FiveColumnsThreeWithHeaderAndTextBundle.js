import React, {useEffect, useState} from 'react';
import EditForm from "./EditForm";
import HeadingWithTextComponent
    from "../../../assets/App/components/multi_use_components/HeadingWithTextComponent";
import ComponentMenu from "../../../assets/App/components/multi_use_components/ComponentMenu";
import SectionFormBase from "../../../assets/App/components/base_components/SectionFormBase";

const FiveColumnsThreeWithHeaderAndTextBundle = ({params, id, components}) =>{
    const [heading, setHeading] = useState('')
    const [description, setDescription] = useState('')

    const [firstTileFirstText, setFirstTileFirstText] = useState('')
    const [firstTileSecondText, setFirstTileSecondText] = useState('')

    const [secondTileFirstText, setSecondTileFirstText] = useState('')
    const [secondTileSecondText, setSecondTileSecondText] = useState('')

    const [thirdTileFirstText, setThirdTileFirstText] = useState('')
    const [thirdTileSecondText, setThirdTileSecondText] = useState('')

    const [showEditForm, setShowEditForm] = useState(false)
    useEffect(() => {
        params[0].elements.map((el=> {
            switch (el.name) {
                case 'Nagłówek':
                    return setHeading(el.text)
                case 'Główny opis':
                    return setDescription(el.text)
                case 'Pierwszy box':
                    setFirstTileFirstText(el.firstText)
                    return setFirstTileSecondText(el.secondText)
                case 'Drugi box':
                    setSecondTileFirstText(el.firstText)
                    return setSecondTileSecondText(el.secondText)
                case 'Trzeci box':
                    setThirdTileFirstText(el.firstText)
                    return setThirdTileSecondText(el.secondText)
                default:
                    return null
            }
        }))
    },[])

    return(
        <>
            <section className={'FiveColumnsThreeWithHeaderAndTextBundle'} style={{display:"flex"}}>
                <div style={{display:"flex"}}>
                    <h1>{heading}</h1>
                    <p>{description}</p>
                </div>
                { firstTileFirstText || secondTileFirstText || thirdTileFirstText !== '' ?
                    <div style={{display:"flex"}}>
                        {firstTileFirstText !== '' ? <HeadingWithTextComponent  heading={firstTileFirstText}  text={firstTileSecondText}  containerClassName={'HeaderWithTextAndThreeNumberWithTextFirstTile'} /> : null}
                        {secondTileFirstText !== '' ? <HeadingWithTextComponent heading={secondTileFirstText} text={secondTileSecondText}  containerClassName={'HeaderWithTextAndThreeNumberWithTextSecondTile'} /> : null}
                        {thirdTileFirstText !== '' ? <HeadingWithTextComponent heading={thirdTileFirstText} text={thirdTileSecondText}  containerClassName={'HeaderWithTextAndThreeNumberWithTextThirdTile'} /> : null}
                    </div>
                    : null}

                <ComponentMenu id={id} components={components} editFunction={e=>setShowEditForm(true)}/>
            </section>
            {/*{showEditForm === true ? <EditForm id={id}/> : null}*/}
            {showEditForm === true ? <SectionFormBase closeBtnFucntion={e => setShowEditForm(false)}
                                                      title={'FiveColumnsThreeWithHeaderAndTextBundle'}
                                                      formContainerClass={''}
                                                      form={<EditForm id={id}/>}
            /> : null }
        </>
    )
}

export default FiveColumnsThreeWithHeaderAndTextBundle;