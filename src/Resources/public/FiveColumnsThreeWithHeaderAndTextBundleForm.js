import React, {useEffect, useState} from 'react';
import ExamplePicture from './ExamplePicture.png'
import Icon from './icon.png'
import SectionTile from "../../../assets/App/components/multi_use_components/SectionTile";
import SectionExampleImage from "../../../assets/App/components/multi_use_components/SectionExampleImage";
import SectionFormBase from '../../../assets/App/components/base_components/SectionFormBase'
import SectionForm from "./SectionForm";


const FiveColumnsThreeWithHeaderAndTextBundleForm = ({currentMenuItem, sectionList, pagesList}) => {

    const [showForm, setShowForm] = useState(false)
    const [showExampleImage, setShowExampleImage] = useState(false)
    const [bundleName, setBundleName] = useState('BannerBundle')
    const [title, setTitle]= useState('Sekcja 1')
    return (
    <>

    <SectionTile
        title={title}
        icon={Icon}
        description={'Lorem ipsum ...'}
        showExampleImgFunction={e=>setShowExampleImage(true)}
        showSectionFormFunction={e=>setShowForm(true)}
    />
        {showExampleImage === true ? <SectionExampleImage img={ExamplePicture} closeFunction={e=>setShowExampleImage(false)} /> : null }
        {showForm === true ? <SectionFormBase closeBtnFucntion={e=>setShowForm(false)}
                                              title={title}
                                              formContainerClass={''}
                                              form={<SectionForm item={null}
                                                                 sectionList={sectionList}
                                                                 currentMenuItem={currentMenuItem}
                                                                 pagesList={pagesList}
                                                    />}
        /> : null }
    </>
    )
}
export default FiveColumnsThreeWithHeaderAndTextBundleForm
