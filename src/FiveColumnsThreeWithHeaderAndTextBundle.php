<?php

namespace FiveColumnsThreeWithHeaderAndTextBundle;

use FiveColumnsThreeWithHeaderAndTextBundle\DependencyInjection\FiveColumnsThreeWithHeaderAndTextBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FiveColumnsThreeWithHeaderAndTextBundle extends Bundle
{
    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new FiveColumnsThreeWithHeaderAndTextBundleExtension();
        }
        return $this->extension;
    }
}