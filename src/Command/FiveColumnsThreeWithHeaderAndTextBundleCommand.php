<?php
namespace FiveColumnsThreeWithHeaderAndTextBundle\Command;

use App\Entity\Section;
use App\Repository\SectionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FiveColumnsThreeWithHeaderAndTextBundleCommand extends Command
{
    private $em;
    private $sectionRepository;

    public function __construct(EntityManagerInterface $em, SectionRepository $sectionRepository)
    {
        $this->em = $em;
        $this->sectionRepository = $sectionRepository;

        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'coaladmin:create-FiveColumnsThreeWithHeaderAndTextBundle';

    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new section with five columns, the last three columns consist of header and text.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $section = $this->sectionRepository->findOneBy(['title' => 'FiveColumnsThreeWithHeaderAndTextBundle']);

        if (!$section) {
            $section = new Section();
        }

        $section
            ->setTitle('FiveColumnsThreeWithHeaderAndTextBundle')
            ->setParams($this->getParams())
            ->setTemplateUrl('@FiveColumnsThreeWithHeaderAndTextBundle/template/show.html.twig')
        ;

        $this->em->persist($section);
        $this->em->flush();

        return Command::SUCCESS;
    }

    private function getParams()
    {
        $params = [
            'elements'=> [
                [
                    "id" => 1,
                    "name" => "Nagłówek",
                    "text" => null
                ],
                [
                    "id" => 2,
                    "name" => "Główny opis",
                    "text" => null
                ],
                [
                    "id" => 3,
                    "name" => "Pierwszy box",
                    "firstText" => null,
                    "secondText" => null
                ],
                [
                    "id" => 4,
                    "name" => "Drugi box",
                    "firstText" => null,
                    "secondText" => null
                ],
                [
                    "id" => 5,
                    "name" => "Trzeci box",
                    "firstText" => null,
                    "secondText" => null
                ],
            ]
        ];

        return $params;
    }
}